var metadata = {
  createdAt: {
    type: Date,
    required: true,
    default: () => { return '' + Date.now() }
  },
  updatedAt: {
    type: Date,
    required: true,
    default: () => { return '' + Date.now() }
  },
  active: {
    type: Boolean,
    default: true
  }
};

module.exports.Metadata = metadata;
