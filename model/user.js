var mongoose = require("mongoose");
var bcrypt = require("bcryptjs");
var metadata = require("./metadata");

var UserSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: true,
    maxlength: 200
  },
  lastname: {
    type: String,
    required: true,
    maxlength: 200
  },
  email: {
    type: String,
    unique: true,
    required: true,
    maxlength: 200
  },
  password: {
    type: String,
    maxlength: 200
  },
  phoneNumber: {
    type: String,
    maxlength: 12
  }
});

UserSchema.add(metadata.Metadata);

UserSchema.pre("save", function(callback) {
  let user = this;
  console.log("pre: " + user)
  // Break out if the password hasn't changed
  if(typeof user.password == 'undefined') return callback();
  if(user.password == null) return callback();
  if (!user.isModified("password")) return callback();

  // Password changed so we need to hash it
  bcrypt.genSalt(10, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return callback(err);
      user.password = hash;
      callback();
    });
  });
});

module.exports = mongoose.model("User", UserSchema, "user");
