const check = require("express-validator").check;

const emailValidator = check("email")
  .isEmail()
  .withMessage("email is not in email format");

const passwordValidator = check("password")
.optional()
.not()
.isEmpty()
.withMessage("password should not be empty");

const phoneValidator = check("phoneNumber")
.optional()
.matches("^(?:[0-9]{10}|)$")
.withMessage("Enter a valid phone number");

const firstnameValidator = check("firstname")
.not()
.isEmpty()
.withMessage("firstname should not be empty");

const lastnameValidator = check("lastname")
.not()
.isEmpty()
.withMessage("lastname should not be empty");

const activeValidator = check("active")
.optional()
.isBoolean();

const idValidator = check("id")
.not()
.isEmpty()
.withMessage("id should not be empty");

module.exports.emailValidator = emailValidator;
module.exports.passwordValidator = passwordValidator;
module.exports.phoneValidator = phoneValidator;
module.exports.firstnameValidator = firstnameValidator;
module.exports.lastnameValidator = lastnameValidator;
module.exports.idValidator = idValidator;
module.exports.activeValidator = activeValidator;