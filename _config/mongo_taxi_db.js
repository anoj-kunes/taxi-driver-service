let mongoose = require("mongoose");
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true 
  });
  
  let db = mongoose.connection;
  
  if (!db) {
    console.error("Error connecting db");
  } else {
    console.log("DB connected successfully");
  }
