let express = require("express");
var app = express();
let bodyParser = require("body-parser");
let routes = require("../api_routes/routes");

// require('custom-env').env(true)

require('./mongo_taxi_db')

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// CORS Filter
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(bodyParser.json());

var port = process.env.PORT;

app.use("/api/v1", routes);

app.get("/", (req, res) => res.send("Taxi Service with express " + process.env.APP_NAME));

app.listen(port, function() {
  console.log("Running REST-API on port " + port);
});