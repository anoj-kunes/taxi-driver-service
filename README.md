# Taxi Driver Service
My first simple Nodejs express application which provides REST APIs .

Project depends on Mongodb.

To run service:
~~~
$env:PORT=9998; $env:APP_NAME="Local Node App" ; $env:MONGO_URI="mongodb://localhost:27017/taxi_db" ; nodemon index.js
~~~

## Postman API
Includes Api endpoints for taxi-service
~~~
https://www.getpostman.com/collections/eae7bb39aa052c370d6b
~~~

## Release Notes
### 1.0.0

- initial version with Api endpoints which includes create, update, delete and get all Users

### Upcoming Releases

- create more endpoints for taxi-driver-details and taxi transaction details
- using JWT to authorize endpoints