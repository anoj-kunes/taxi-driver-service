var User = require("../model/user");
var objectEmpty = require("../util/emptyObjectUtil");
const validationResult = require("express-validator").validationResult;
const Joi = require("joi");
var _ = require("lodash");

// Create endpoint /api/v1/users for POST
exports.createUser = function(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res
      .status(422)
      .json({ type: "VALIDATION_FAILED", errors: errors.array() });
  }

  // validate user
  if (!req.body) {
    throw res.status(400).send({
      type: "EMPTY_CONTENT_ERROR",
      errors: [{ error: "User content can not be empty" }]
    });
  }

  // prepare user details
  var user = new User({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
    phoneNumber: req.body.phoneNumber
  });

  // save user details
  // move to service
  user
    .save()
    .then(data => {
      console.log(data);
      res.status(200).json(convertUser(data));
    })
    .catch(err => {
      console.log(err);
      throw res.status(400).json({
        type: "INTERNAL_SERVER_ERROR",
        errors: [
          {
            error: err.message || "Some error occurred while creating the User."
          }
        ]
      });
    });
};

// Create endpoint /api/v1/users for GET ALL
exports.getUsers = function(req, res) {
  const querySchema = {
    page: Joi.number()
      .optional()
      .error(new Error("page param must be a number")),
    limit: Joi.number()
      .optional()
      .error(new Error("limit param must be a number")),
    email: Joi.string()
      .optional()
      .error(new Error("email param must be a string")),
    active: Joi.boolean()
      .optional()
      .error(new Error("active param must be a boolean")),
    fullname: Joi.string()
      .optional()
      .error(new Error("fullname param must be a string"))
  };
  Joi.validate(req.query, querySchema, (err, values) => {
    console.log({ values, err });
    if (err) throw res.status(400).json({ type: "VALIDATION_FAILED", errors: [{error: err.message }] });
  });
  var pagination = [];
  var page = req.query.page;
  var limit = req.query.limit;

  if (parseInt(limit) === 0) {
    let response = { type: "VALIDATION_FAILED", errors: [{error: "limit must be positive"}] };
    throw res.status(400).json(response);
  }

  if (
    typeof page != "undefined" &&
    page != null &&
    typeof limit != "undefined" &&
    limit != null
  ) {
    pagination.push({ $skip: parseInt(limit) * (parseInt(page) - 1) });
  }

  if (typeof limit != "undefined" && limit != null) {
    pagination.push({ $limit: parseInt(limit) });
  }

  if (parseInt(page) < 0 || parseInt(page) === 0) {
    let response = {
      type: "VALIDATION_FAILED",
      errors: [{error: "invalid page number, should start with 1"}]
    };
    throw res.status(400).json(response);
  }

  var criteria = {};

  if (typeof req.query.email != "undefined" && req.query.email != null) {
    criteria.email = { $regex: new RegExp(req.query.email) };
  }

  if (typeof req.query.fullname != "undefined" && req.query.fullname != null) {
    criteria.fullname = {
      $regex: new RegExp(req.query.fullname),
      $options: "-i"
    };
  }

  if (typeof req.query.active != "undefined" && req.query.active != null) {
    criteria.active = req.query.active == "true";
  }

  var pipeline = [
    { $addFields: { fullname: { $concat: ["$firstname", " ", "$lastname"] } } }
  ];
  if (!objectEmpty.isEmpty(criteria)) {
    pipeline.push({ $match: criteria });
  }

  const countPipeline = pipeline.map(result => result);
  if (
    typeof pagination != "undefined" &&
    pagination != null &&
    pagination.length > 0
  ) {
    pipeline = pipeline.concat(pagination);
  }

  countPipeline.push({ $group: { _id: null, count: { $sum: 1 } } })
  console.log({ countPipeline });
  console.log({ pipeline });

  User.aggregate(countPipeline, function(countErr, count) {
    if (countErr) throw res.status(500).json({ message: countErr.message });
    User.aggregate(pipeline, function(err, results) {
      if (err) throw res.status(500).json({ message: err.message });
      console.log({count});
      let totalRecords = typeof count != 'undefined' && count != null && count.length > 0 && typeof count[0].count != 'undefined' && count[0].count != null ? count[0].count : 0;
      let response = {page: parseInt(page), totalRecords: totalRecords, limit: parseInt(limit), results: results.map(elem => convertUser(elem))};
      return res.status(200).json(response);
    }).catch(err => {
      let response = {
        type: "INTERNAL_SERVER_ERROR",
        errors: [{error: err.message}]
      };
      throw res.status(500).json(response);
     });
  }).catch(err => {
    let response = {
      type: "INTERNAL_SERVER_ERROR",
      errors: [{error: err.message}]
    };
    throw res.status(500).json(response);
   });
};

// Create endpoint /api/v1/users/:id for GET
exports.getUser = function(req, res) {
  var id = req.params.id;
  User.findById(id)
    .then(user => {
      if (!user) {
        throw res.status(404).json({
          type: "RESOURCE_NOT_FOUND",
          errors: [{ error: "User not found for #" + id }]
        });
      }
      return res.status(200).json(convertUser(user));
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        throw res.status(404).json({
          type: "RESOURCE_NOT_FOUND",
          errors: [{ error: "User not found for #" + id }]
        });
      }
      throw res.status(500).json({
        type: "INTERNAL_SERVER_ERROR",
        errors: [{ error: "User not found for #" + id }]
      });
    });
};

// Create endpoint /api/v1/users for PUT
exports.updateUser = function(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res
      .status(422)
      .json({ type: "VALIDATION_FAILED", errors: errors.array() });
  }

  if (!req.body) {
    throw res.status(400).json({
      type: "EMPTY_CONTENT_ERROR",
      errors: [{ error: "User content can not be empty" }]
    });
  }

  // prepare user details
  var userbody = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    username: req.body.username,
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    active: req.body.active,
    updatedAt: new Date(),
    id: req.body.id
  };

  if (typeof req.body.password != "undefined" && req.body.password != null) {
    userbody.password = req.body.password;
  }

  let id = req.body.id;

  User.findById(id)
    .then(user => {
      if (!user) {
        throw res.status(404).json({
          type: "RESOURCE_NOT_FOUND",
          errors: [{ error: "User not found for #" + id }]
        });
      }

      if (typeof req.body.active == "undefined") {
        userbody.active = user.active;
      }

      if(typeof req.body.active != "undefined" && req.body.active == null) {
        userbody.active = user.active;
      }
      _.assign(user, userbody);

      user.save(function(err, data) {
        if (err) {
          throw res.status(500).json({
            type: "INTERNAL_SERVER_ERROR",
            errors: [
              {
                error:
                  err.message || "Some error occurred while updating the User."
              }
            ]
          });
        }
        return res.status(200).json(convertUser(data));
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        throw res.status(404).json({
          type: "RESOURCE_NOT_FOUND",
          errors: [{ error: "User not found for #" + id }]
        });
      }
      throw res.status(500).json({
        type: "INTERNAL_SERVER_ERROR",
        errors: [{ error: "Error retrieving user with id " + id }]
      });
    });
};

// Create endpoint /api/v1/users/:id for DELETE
exports.deleteUser = function(req, res) {
  var id = req.params.id;
  User.findById(id)
    .then(user => {
      if (!user) {
        throw res.status(404).json({
          type: "RESOURCE_NOT_FOUND",
          errors: [{ error: "User not found for #" + id }]
        });
      }

      user.active = false;

      user.save(function(err, data) {
        if (err) {
          throw res.status(500).json({
            type: "INTERNAL_SERVER_ERROR",
            errors: [
              {
                error:
                  err.message || "Some error occurred while deleting the User."
              }
            ]
          });
        }
      });

      return res.status(200).json(convertUser(user));
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        throw res.status(404).json({
          type: "RESOURCE_NOT_FOUND",
          errors: [{ error: "User not found for #" + id }]
        });
      }
      throw res.status(500).json({
        type: "INTERNAL_SERVER_ERROR",
        errors: [{ error: "User not found for #" + id }]
      });
    });
};

function convertUser(user) {
  let convertedUser = {};
  convertedUser.firstname = user.firstname;
  convertedUser.lastname = user.lastname;
  convertedUser.username = user.username;
  convertedUser.email = user.email;
  convertedUser.id = user._id;
  convertedUser.phoneNumber = user.phoneNumber;
  convertedUser.createdAt = user.createdAt;
  convertedUser.updatedAt = user.updatedAt;
  convertedUser.active = user.active;
  return convertedUser;
}
