var apiRoutes = require('./api-routes');
var userRoutes = require('./user-router');

module.exports = [apiRoutes, userRoutes]