let router = require('express').Router();

router.get('/', function(req, res){
    return res.status(200).json({
        status: 'REST-API working nodeman',
        message: 'Welcome to REST-API'
    })
});

module.exports = router;