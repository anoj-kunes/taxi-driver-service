let router = require("express").Router();
const validator = require("../validator/userValidator");
var userController = require("../controller/userController");

router.route("/users").post(
  [
    validator.lastnameValidator,
    validator.passwordValidator,
    validator.phoneValidator,
    validator.firstnameValidator,
    validator.emailValidator,
    validator.activeValidator
  ],
  userController.createUser
);

router.route("/users").put(
  [
    validator.idValidator,
    validator.lastnameValidator,
    validator.passwordValidator,
    validator.phoneValidator,
    validator.firstnameValidator,
    validator.emailValidator,
    validator.activeValidator
  ],
  userController.updateUser
);

router.route("/users/:id").get(userController.getUser);

router.route("/users/:id").delete(userController.deleteUser);

router.route("/users").get(userController.getUsers);

module.exports = router;
